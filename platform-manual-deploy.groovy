@Library('deploy-pipeline-shared-libraries')

import com.odigeo.pipeline.deploy.DeployTypeFactory
import com.odigeo.pipeline.gcproject.GcProject
import com.odigeo.pipeline.gcproject.GcProjectRetriever

pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    environment {
        JIRA_SITE = 'odigeo_jira'
        DOCKER_GOOGLE_CREDENTIALS = "${WORKSPACE}/.config/google-cloud/service_account.json"
        DEPLOY_K8S_IMAGE_DEFAULT = "781302463931.dkr.ecr.eu-west-1.amazonaws.com/deploy-k8s:2.4.0-rc7"
        DEPLOY_TIMEOUT_SECONDS = "2400"
        HELM_SLACK_TOKEN = "T4PDJ6ARK/BGKN67WU8/lnU1cGvZ9KelKGoB5u8M1u0U";
        DEPLOY_REPORTER_URL = "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"

        toDeployCanaryIssueId = null
        toDeployStableIssueId = null
        userId = null
        currentCanaryIssue = null;
        currentStableIssue = null;
        canaryVersion = null;
        stableVersion = null;
    }
    parameters {
        string(defaultValue: '',description: 'module to deploy', name: 'module')
        string(defaultValue: '',description: 'version to deploy', name: 'version')
        string(defaultValue: '', description: 'prod projects where deploy (separated with , )', name: 'prodProjects')
        string(defaultValue: '', description: 'test projects where deploy (separated with , )', name: 'testProjects')
        choice(choices: ['Default', 'SparkRobot'], description: 'Deploy Type', name: 'deployType')
    }
    stages {
        stage('Set up') {
            steps {
                script {
                    downloadDockerLoginToken()
                }
                wrap([$class: 'BuildUser']) {
                    script {
                        if (env.BUILD_USER_ID) {
                            userId = BUILD_USER_ID
                        } else {
                            userId = "release_management"
                        }
                    }
                }
                script {
                    if (env.DEPLOY_K8S_IMAGE_EXTERNAL) {
                        DEPLOY_K8S_IMAGE = DEPLOY_K8S_IMAGE_EXTERNAL
                    } else {
                        DEPLOY_K8S_IMAGE = DEPLOY_K8S_IMAGE_DEFAULT
                    }
                    echo "select DEPLOY_K8S_IMAGE: $DEPLOY_K8S_IMAGE"
                    deployType = new DeployTypeFactory(steps).getInstance(env.deployType)
                }
            }
        }
        stage('Checkout') {
            steps {
                checkoutRepository("${artifactId}-platform")
                script {
                    lastTag = getLastTag()
                    echo "last Tag: ${lastTag}"
                }

            }
        }
        stage('Deploy') {
            stages {
                stage('Get deployment versions') {
                    steps {
                        script {
                            if (deployType.hasCanary()) {
                                canaryVersion = deployType.getCanaryVersionFromDeploymentFile("prod")
                                echo "canary: ${canaryVersion}"
                            }
                            stableVersion = deployType.getStableVersionFromDeploymentFile("prod")
                            echo "stable: ${stableVersion}"
                        }
                    }
                }
                stage('Get Jira issues') {
                    steps {
                        script {
                            try {
                                if (deployType.hasCanary()) {
                                    toDeployCanaryIssueId = getOrCreateDeploymentJira(canaryVersion)
                                    currentCanaryIssue = getDeploymentJiraFromStatus('Canary')
                                }
                                toDeployStableIssueId = getOrCreateDeploymentJira(stableVersion)
                                currentStableIssue = getDeploymentJiraFromStatus('Stable')
                            } catch (ex) {
                                echo "problems with jira integration"
                                ex.getMessage()
                                currentBuild.result = 'UNSTABLE'
                            }

                        }
                    }
                }
                stage('Deploy to Prod') {
                    when { expression { params.deployToProd == true } }
                    steps {
                        script {
                            List<GcProject> projects = getGcProject("prod", "edo-prod01")
                            parallel generateDeployStagesForAllProjects(projects, "prod", artifactId, helmReleaseName, DEPLOY_TIMEOUT_SECONDS, HELM_SLACK_TOKEN, DEPLOY_K8S_IMAGE)
                        }
                    }
                    post {
                        success {
                            script {
                                sendFinishDeployEvent(artifactId, "OK", stableVersion, canaryVersion, "PROD", lastTag, DEPLOY_REPORTER_URL)
                                if (deployType.hasCanary()) {
                                    successProdDeployActions(toDeployCanaryIssueId, canaryVersion, 'Canary', currentCanaryIssue, stableVersion)
                                }
                                successProdDeployActions(toDeployStableIssueId, stableVersion, 'Stable', currentStableIssue, stableVersion)
                            }
                        }
                        failure {
                            script {
                                sendFinishDeployEvent(artifactId, "KO", stableVersion, canaryVersion, "PROD", lastTag, DEPLOY_REPORTER_URL)
                                errorDeployActions(toDeployCanaryIssueId, "Prod")
                            }
                        }
                    }
                }
                stage('Integration-qa') {
                    when {
                        environment name: 'deployToIntegration-qa', value: 'true'
                    }
                    steps {
                        build job: "deploy-integration-qa", parameters: [
                                string(name: 'module', value: artifactId),
                                string(name: 'version', value: stableVersion)
                        ]
                    }
                }
                stage('Deploy to Test') {
                    when { expression { params.deployToTest == true } }
                    steps {
                        script {
                            List<GcProject> projects =getGcProject("test", "edo-test01")
                            parallel generateDeployStagesForAllProjects(projects , "test", artifactId, helmReleaseName, DEPLOY_TIMEOUT_SECONDS, HELM_SLACK_TOKEN, DEPLOY_K8S_IMAGE)
                        }
                    }
                    post {
                        success {
                            script {
                                sendFinishDeployEvent(artifactId, "OK", stableVersion, canaryVersion, "TEST", lastTag, DEPLOY_REPORTER_URL)
                                echo "Adding comments to jiras"
                                addCommentSuccessDeploy(toDeployStableIssueId, 'Test')
                            }
                        }
                        failure {
                            script {
                                sendFinishDeployEvent(artifactId, "KO", stableVersion, canaryVersion, "TEST", lastTag, DEPLOY_REPORTER_URL)
                                echo "Adding comments to jiras"
                                addCommentFailedDeploy(toDeployStableIssueId, 'Test')
                            }
                        }
                    }
                }
            }
        }

    }
}
def getGcProject(String environment, String defaultProject) {
    GcProjectRetriever gcProjectRetriever = new GcProjectRetriever()
    return gcProjectRetriever.getProjects(getPropertyFromPipelineConfiguration("${environment}Projects", defaultProject))
}

def sendFinishDeployEvent(String module, String status, String stableVersion, String canaryVersion, String environment, String tag, String deployReporterUrl) {
    sendEventToDeployReporter(module, "DEPLOY-" + status + "-" + environment, stableVersion, canaryVersion, "Deploy finished with ${status}  staus; Platform Tag: " + tag, deployReporterUrl)
}


def getTransition(issueId, transitionName) {
    def transitionResult = jiraGetIssueTransitions(idOrKey: issueId, failOnError: false)
    def transitions = transitionResult.data.transitions
    for (j = 0; j < transitions.size(); j++) {
        if (transitions[j].name == transitionName) {
            return [transition: transitions[j]]
        }
    }
    return null
}

def moveToStatus(issueId, status) {
    try {
        def transitionInput = getTransition(issueId, status)
        jiraTransitionIssue(idOrKey: issueId, input: transitionInput, failOnError: false)
        echo "${issueId} moved to ${status}"
    } catch (ex) {
        echo "cannot move ${issueId} to ${status}"
        ex.getMessage()
        currentBuild.result = 'UNSTABLE'
    }
}


def moveDeploymentJiraFromOneStatusToAnother(issue, String initialStatus, String finalStatus) {
    moveToStatus(issue.key, finalStatus)
    addCommentToJira(issue.key, "[~${userId}] move to '${finalStatus}' with this Job execution: ${env.BUILD_URL}")
    echo "moved ${issue.key} from ${initialStatus} to ${finalStatus}"
}

def getOrCreateDeploymentJira(version) {
    try {
        def issue = getDeploymentJiraFromVersion(version)
        if (issue != null) {
            return issue.key
        }
    } catch (ex) {
        echo "cannot get jira for this version, try to create one"
        ex.getMessage()
    }
    try {
        createFixVersionIfNecessary(version)
        return createDeploymentJira(version, null)
    } catch (ex) {
        echo "cannot create jira for this version"
        ex.getMessage()
        echo e.getMessage()
        currentBuild.result = 'UNSTABLE'
        return null
    }
}

def addCommentSuccessDeploy(issueId, partition) {
    addCommentToJira(issueId, "[~${userId}] deployed the module to ${partition} with this Job execution: ${env.BUILD_URL}")
}

def addCommentFailedDeploy(issueId, partition) {
    addCommentToJira(issueId, "[~${userId}] failed when deploy to ${partition} with this Job execution: ${env.BUILD_URL}")
}

def successProdDeployActions(deployedIssueId, String deployedVersion, String partition, previousIssue, String stableVersion) {
    echo "deployedIssueId: ${deployedIssueId}"
    echo "deployedVersion: ${deployedVersion}"
    echo "partition: ${partition}"
    echo "previousIssue: ${stableVersion}"
    echo "stableVersion: ${stableVersion}"
    if (deployedIssueId != null) {
        if (deployedVersion != getVersionFromIssue(previousIssue)) {
            echo "Moving previous jira to 'Archived'"
            moveIssueToArchivedIfExist(previousIssue, partition)
            moveIssueToCorrectStatus(partition, deployedVersion, deployedIssueId, stableVersion)
            generateRollbackSubtaskIfNeeded(deployedVersion, previousIssue, partition)
        }
        addCommentSuccessDeploy(deployedIssueId, partition)
    } else {
        echo "cannot perform the Jira operation"
    }
}

def getVersionFromIssue(issue) {
    if (issue != null) {
        return issue.fields.summary
    } else {
        return null
    }
}

def moveIssueToArchivedIfExist(issue, String partition) {
    if (issue != null) {
        moveDeploymentJiraFromOneStatusToAnother(issue, partition, 'Archived')
    }
}

def generateRollbackSubtaskIfNeeded(String deployedVersion, previousIssue, String partition) {
    // if the deployed version > than previous version is a rollback
    if (previousIssue != null && versionComparator(deployedVersion, previousIssue.fields.summary) < 0) {
        createRollbackSubtask(deployedVersion, previousIssue.fields.summary, partition, previousIssue.key)
    }
}

def moveIssueToCorrectStatus(String partition, String deployedVersion, String deployedIssueId, String stableVersion) {
    if (!(partition == "Canary" && deployedVersion == stableVersion)) {
        echo "Moving jira to ${partition} and adding a comment"
        moveToStatus(deployedIssueId, partition)
    }
}

def errorDeployActions(issueId, partition) {
    if (issueId != null) {
        echo "Adding comments to jiras"
        addCommentFailedDeploy(issueId, partition)
    } else {
        echo "cannot perform the Jira operation"
    }
}

def versionComparator(a, b) {
    List verA = a.tokenize('.')
    List verB = b.tokenize('.')

    def commonIndices = Math.min(verA.size(), verB.size())

    for (int i = 0; i < commonIndices; ++i) {
        def numA = verA[i].toInteger()
        def numB = verB[i].toInteger()
        if (numA != numB) {
            return numA <=> numB
        }
    }
    // If we got this far then all the common indices are identical, so whichever version is longer must be more recent
    return verA.size() <=> verB.size()
}

def createRollbackSubtask(deployVersion, rollbackVersion, partition, parentKey) {

    def newIssue = [fields: [
            project    : [key: "${jiraProjectId}"],
            summary    : "Rollback of version ${rollbackVersion} with version ${deployVersion}",
            description: "Rollback of version ${rollbackVersion} with version ${deployVersion} in partition ${partition}",
            parent     : ["key": "${parentKey}"],
            labels     : ["Rollback-${partition}"],
            issuetype  : [name: 'Task']]]

    newJiraResponse = jiraNewIssue(issue: newIssue, failOnError: false)
    def issueKey = newJiraResponse.data.key

    echo "Create rollback task ${issueKey} for version ${rollbackVersion}"
}