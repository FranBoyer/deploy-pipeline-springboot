@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory

pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    parameters {
        string(defaultValue: '',description: 'module to deploy', name: 'module')
        choice(choices: ['prod', 'test'], description: 'Environment to restart', name: 'environment')

    }
    environment {
        DEPLOY_K8S_IMAGE_DEFAULT = "781302463931.dkr.ecr.eu-west-1.amazonaws.com/deploy-k8s:2.4.0-rc6"
        TEST_GCP_PROJECT = "edo-test01"
        TEST_GCP_COMPUTE_ZONE = "europe-west3"
        TEST_GKE_CLUSTER_NAME = "gke1"
        PROD_GCP_PROJECT = "edo-prod01"
        PROD_GCP_COMPUTE_ZONE = "europe-west3"
        PROD_GKE_CLUSTER_NAME = "gke1"
    }
    stages {
        stage('Set up') {
            steps {
                script {
                    downloadDockerLoginToken()
                    if(environment == "prod"){
                        gcpProject=PROD_GCP_PROJECT
                        gpcComputeZone=PROD_GCP_COMPUTE_ZONE
                        gkeClusterName=PROD_GKE_CLUSTER_NAME
                    }else{
                        gcpProject=TEST_GCP_PROJECT
                        gpcComputeZone=TEST_GCP_COMPUTE_ZONE
                        gkeClusterName=TEST_GKE_CLUSTER_NAME
                    }
                }
            }
        }
        stage('Restart') {
            steps {
                 script{
                     setServiceAccountFile("edo-${environment}-google-cloud")
                     sh "docker run -v ${WORKSPACE}/.config/google-cloud/service_account.json:/deploy-k8s/gcp_service_account/service_account.json -e GKE_DEPLOY=true -e GCP_PROJECT=${gcpProject} -e GCP_COMPUTE_ZONE=${gpcComputeZone} -e GKE_CLUSTER_NAME=${gkeClusterName} -e GKE_APPLICATION=${module} -e EDO_RESTART=true -e K8S_DEPLOY_REQUEST_TIMEOUT=3600 ${DEPLOY_K8S_IMAGE_DEFAULT}"
                 }
            }
        }
    }
}
