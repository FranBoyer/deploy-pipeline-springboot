@Library('deploy-pipeline-shared-libraries')_

pipeline {
    agent any
    parameters {
        string(description: 'Module name', name: 'module_name')
        string(description: 'First version number.', name: 'major_version')
        string(description: 'Second version number.', name: 'minor_version')
        booleanParam(defaultValue: 'true', description: 'execute create release candidate stage', name: 'create_release_candidate')

    }
    stages {
        stage('Setup') {
            steps {
                script{
                    moduleName = module_name
                    prodForkName = 'prod-' + major_version + '.' + minor_version
                    newMinorVersion = minor_version.toInteger() +1
                    newUpstreamVersion = major_version + '.' + newMinorVersion + '.0-SNAPSHOT'
                    prodJobName = module_name + '_' + prodForkName
                }
            }
        }
        stage('Create production fork & job') {
            steps{
                build (job: 'pipeline-forktool-creation', parameters: [[$class: 'StringParameterValue', name: 'module_name', value: moduleName], [$class: 'StringParameterValue', name: 'from_fork_name', value: moduleName], [$class: 'StringParameterValue', name: 'new_fork_name', value: prodForkName]] )
            }
        }
        stage('Increase upstream version') {
            steps{
                build (job: 'pipeline-version-set', parameters: [[$class: 'StringParameterValue', name: 'module_name', value: moduleName], [$class: 'StringParameterValue', name: 'version', value: newUpstreamVersion]] )
            }
        }
        stage('Wait until production job is created') {
            steps{
                script{
                    echo "prodJobName: ${prodJobName}"
                    timeout(time: 600, unit: 'SECONDS') {
                        while(jenkins.model.Jenkins.instance.getItem(prodJobName) == null){
                            echo "waiting prod job"
                            sleep(5)
                        }
                    }
                }

            }

        }
        stage('Create release candidate') {
            when { expression { params.create_release_candidate == true } }
            steps{
                build (job: 'rm_create-release-candidate-git', parameters: [[$class: 'StringParameterValue', name: 'project_name', value: moduleName], [$class: 'StringParameterValue', name: 'branch_name', value: prodForkName]] )
            }
        }


    }
}











