@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory
import com.odigeo.pipeline.vcs.VcsFactory
import com.odigeo.pipeline.vcs.Vcs

pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    parameters {
        string(defaultValue: '',description: 'module to deploy', name: 'module')
        string(defaultValue: '',description: 'version to deploy', name: 'version')
        choice(choices: ['Default', 'SparkRobot'], description: 'Deploy Type', name: 'deployType')


    }
    environment {
        DEPLOY_REPORTER_URL= "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"
    }
    stages {
        stage('Checkout') {
            steps {
                script{
                    vcs = new VcsFactory(steps).getInstance("${module}-platform")
                    vcs.checkout("${module}-platform")
                }

            }
        }
        stage('Force Platform Deploy') {
            steps {
                script {
                    deployType = new DeployTypeFactory(steps).getInstance(deployType)
                    if (deployType.hasCanary()) {
                        deployType.updateCanaryDeploymentFile("test", version)
                    }
                    deployType.updateStableDeploymentFile("test",version)
                    vcs.commitAndPushIfChanges("set ${version} in Test with force-deploy-to-test")
                    build job: "${module}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToProd', value: false)]
                }
            }
        }
    }
}












