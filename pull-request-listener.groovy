import hudson.model.*

pipeline {
	agent any
	environment {
		JIRA_SITE = 'odigeo_jira'
	}
	triggers {
		GenericTrigger(
				genericVariables: [
						[key: 'REPOSITORY_NAME', value: '$.pullrequest.source.repository.name']
						,[key: 'PULL_REQUEST_STATE', value: '$.pullrequest.state']
				],


				token: 'eXLq4Rcdt8prCMJy',

				printContributedVariables: true,
				printPostContent: true,

				silentResponse: false,

		)
	}
	stages {
		stage ('Move jira') {
			steps {
				script{
				    def repoName =  "$REPOSITORY_NAME"
                    def issueState = "$PULL_REQUEST_STATE" //OPEN | MERGED | DECLINED
                    echo "issueState: ${issueState}"
                    echo "repoName: ${repoName}"
                    if(issueState != "noValue"){ //its a pull request
                        def issueId = getIssueId("$REPOSITORY_NAME")
                        addCommentToIssue(issueId, "Pull Request $PULL_REQUEST_STATE. Please go to ${env.BUILD_URL}.")

                        if (issueState == 'OPEN') {
                            moveIssue(issueId, "Pull Request")
                        }
                        else if (issueState == 'MERGED') {
                            moveIssue(issueId, "Merged")
                            deleteJob(Hudson.instance.items, "$REPOSITORY_NAME")
                            deleteFork("$REPOSITORY_NAME")
                        }
                        else if (issueState == 'DECLINED') {
                            moveIssue(issueId, "DEV")
                        }
                    }
				}
			}
		}
	}
}

def getIssueId(repository_name){
	def (module_name, issue) = repository_name.tokenize('_')
	return issue
}

def moveIssue(issueId, transitionName) {
	def transitionInput = getTransition(issueId, transitionName)
	jiraTransitionIssue(idOrKey: issueId, input: transitionInput, failOnError: true)

	echo "${issueId} moved to ${transitionName}"
}

def getTransition(issueId, transitionName) {
	def transitionResult = jiraGetIssueTransitions(idOrKey: issueId, failOnError: false)
	def transitions = transitionResult.data.transitions
	for (j = 0; j < transitions.size(); j++) {
		if (transitions[j].name == transitionName) {
			return [transition: transitions[j]]
		}
	}
	return null
}

def addCommentToIssue(issueId, comment){
	jiraAddComment(idOrKey: issueId, comment: comment, auditLog: false, failOnError: false)

	echo "Added comment to ${issueId}"
}

def deleteJob(items, jobToDelete) {
	items.each { item ->
		if (item.class.canonicalName != 'com.cloudbees.hudson.plugins.folder.Folder') {
			if (jobToDelete == item.fullName) {
				item.delete()
				echo "Job ${jobToDelete} deleted"
			}
		}
	}
}

def deleteFork(src) {
	def url = "https://bitbucket.org/api/2.0/repositories/odigeoteam/${src}"

	withCredentials([usernameColonPassword(credentialsId: 'bitbucket-release-user', variable: 'bitbucketCredentials')]) {
		String auth = bitbucketCredentials.bytes.encodeBase64().toString()
		httpRequest httpMode: 'DELETE',
				url: "https://bitbucket.org/api/2.0/repositories/odigeoteam/${src}",
				customHeaders: [[name: 'Authorization', value: "Basic $auth"]]
	}
}
