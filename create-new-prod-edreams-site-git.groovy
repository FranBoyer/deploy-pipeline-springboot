pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    parameters {
        string(defaultValue: '',description: 'module to deploy', name: 'module')
    }
    environment {
        DEPLOY_REPORTER_URL= "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"
    }pipeline {
        agent {
            label 'GeneralLibraryInstance'
        }
        parameters {
            string(defaultValue: 'edreams-site',description: 'module name', name: 'module')
            string(defaultValue: 'edreams-site',description: 'First version number. Example: <b>2</b>', name: 'major_version')
            string(defaultValue: 'edreams-site',description: 'Second version number. Example: <b>0</b>', name: 'minor_version')

        }
        environment {
        }
        stages {
            stage('\'Create production fork & job') {
                steps {
                    script{
                        moduleName = module_name
                        prodForkName = 'prod-' + major_version + '.' + minor_version
                        newMinorVersion = minor_version.toInteger() +1
                        newUpstreamVersion = major_version + '.' + newMinorVersion + '.0-SNAPSHOT'
                        prodJobName = module_name + '_' + prodForkName
                    }
                    build (job: 'pipeline-forktool-creation', parameters: [[$class: 'StringParameterValue', name: 'module_name', value: moduleName], [$class: 'StringParameterValue', name: 'from_fork_name', value: moduleName], [$class: 'StringParameterValue', name: 'new_fork_name', value: prodForkName]] )
                }
            }
            stage('Increase upstream version') {
                build (job: 'pipeline-version-set-git', parameters: [[$class: 'StringParameterValue', name: 'module_name', value: moduleName], [$class: 'StringParameterValue', name: 'version', value: newUpstreamVersion]] )
            }
            stage('Wait until production job is created') {
                script{
                    echo "prodJobName: ${prodJobName}"
                    timeout(time: 600, unit: 'SECONDS') {
                        while(jenkins.model.Jenkins.instance.getItem(prodJobName) == null){
                            echo "waiting prod job"
                            sleep(5)
                        }
                    }
                }
            }
            stage('Create release candidate') {
                build (job: 'rm_edreams-site-scripts_create-release-candidate-git', parameters: [[$class: 'StringParameterValue', name: 'project_name', value: moduleName], [$class: 'StringParameterValue', name: 'branch_name', value: prodForkName]] )
            }
            parallel{
                stage('build CI'){
                    build (job: "${prodJobName}")
                }
                stage('execute funcional test') {
                    build (job: 'edreams-site_test-automation-all-api-tests-paralelize_maven-jboss6.4', parameters: [[$class: 'StringParameterValue', name: 'fork_name', value: prodJobName]] )
                }
            }

        }
    }

}