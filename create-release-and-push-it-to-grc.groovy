@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.vcs.VcsFactory
import com.odigeo.pipeline.vcs.Vcs

pipeline {
    agent {
        label 'DeployPipeline'
    }
    parameters {
        string(description: 'fork to use to generate release', name: 'FORKNAME')
        string(description: 'release version', name: 'version')
    }
    environment {
        VAULT_DEV_CREDENTIALS = credentials('jenkins-prod')
        DOCKER_GOOGLE_CREDENTIALS = "${WORKSPACE}/.config/google-cloud/service_account.json"//the docker plugin use it, do not removeit
    }
    stages {
        stage('SetUp') {
            steps {
                writeFile file: "$HOME/vault/userPass.properties", text: "userName=$VAULT_DEV_CREDENTIALS_USR\npassword=$VAULT_DEV_CREDENTIALS_PSW"
            }
        }
        stage('Checkout') {
            steps {
                script{
                    Vcs vcs = new VcsFactory(steps).getInstance(FORKNAME)
                    vcs.checkout(FORKNAME)
                }
            }
        }
        stage('Set Version') {
            steps {
                sh "mvn -e versions:set -DgenerateBackupPoms=false -DnewVersion=$version"
            }
        }
        stage('Install') {
            steps {
                sh "mvn -e -U -B clean install -Pbuild-container-image -Ddocker.image.env=prod -DskipTests=true"
            }
        }
        stage('Deploy prod to GPC prod registry') {
            steps {
                setServiceAccountFile("edo-prod-google-cloud")
                sh "mvn -f packaging/container/pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=prod -Ddocker.registry.url=eu.gcr.io/edo-prod-resources -DskipTests=true"
            }
        }
        stage('Deploy integration to GPC test registry') {
            steps {
                setServiceAccountFile("edo-test01-google-cloud")
                sh "mvn -f packaging/container/pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=integration -Ddocker.registry.url=eu.gcr.io/edo-test-resources -DskipTests=true"
            }
        }
        stage('Deploy qa to GPC test registry') {
            steps {
                sh "mvn -f packaging/container/pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=qa -Ddocker.registry.url=eu.gcr.io/edo-test-resources -DskipTests=true"
            }
        }
        stage('Tag') {
            steps {
                script{
                    sh script: "git tag -a ${version} -m 'Add tag ${version}'"
                    sh script: 'git push --set-upstream origin master --tags'
                }
            }
        }

    }
}



