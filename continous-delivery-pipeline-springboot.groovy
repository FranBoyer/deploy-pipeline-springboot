@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory
import com.odigeo.pipeline.vcs.VcsFactory
import com.odigeo.pipeline.vcs.Vcs

pipeline {
    agent none
    environment {
        VAULT_DEV_CREDENTIALS = credentials('jenkins-prod')
		JIRA_SITE = 'odigeo_jira'
		DEPLOY_REPORTER_URL= "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"

		String repositoryName = null
        String deploymentIssueId = null
		String stableVersion = null
		String cIDate = null
		String funcionalTestDate = null
        String evaluationUrl = null
        Vcs vcs = null

    }
    triggers {
        GenericTrigger(
                genericVariables: [
                        [key: 'tag', value: '$.push.changes[?(@.new.type == "tag")].new.name']
                        ,[key: 'repo', value: '$.repository.name']
                ],


                causeString: "Triggered by push to ${artifactId}",
                token: 'pushToken',

                printContributedVariables: true,
                printPostContent: true,

                silentResponse: false,

                regexpFilterText: '$repo',
                regexpFilterExpression: "^(${artifactId})\$"

        )
    }
    parameters {
        string(defaultValue: '[]', description: 'tag to generate release', name: 'tag')
    }
    stages {
        stage ('Basic actions') {
            agent {
                label "DeployPipeline"
            }
            environment {
                DOCKER_GOOGLE_CREDENTIALS = "${WORKSPACE}/.config/google-cloud/service_account.json"//the docker plugin use it, do not removeit
            }
            stages {
                stage ('Setup') {
                    steps {
                        script{
                            echo "Writing vault credential file"
                        }
                        writeFile file: "$HOME/vault/userPass.properties", text: "userName=$VAULT_DEV_CREDENTIALS_USR\npassword=$VAULT_DEV_CREDENTIALS_PSW"

                        script {
                            echo "Creating 'lastTag' variable"
                            lastTag = null
                            deployType = new DeployTypeFactory(steps).getInstance(env.deployType)
                            repositoryName = getRepositoryName()
                            vcs = new VcsFactory(steps).getInstance(repositoryName)
                        }
                    }
                }
                stage('Checkout') {
                    steps {
                        script{
                            echo "Downloading source"
                            vcs.checkout(repositoryName)
                        }
                    }
                }
                stage('Package and verify') {
                    when {
                        environment name: 'quickPipeline', value: 'false'
                    }
                    steps {
                        script{
                            echo "Packaging and verifying"
                        }
                        sh "mvn -e -U clean install"
                        //save timestamp of CI to put it in the deployment jira
                        script{
                            cIDate = getNowTimestapAsString()
                        }
                    }
                    post {
                        always {
                            script{
                                echo "Publishing test results"
                            }
                            jacoco(execPattern: '**/*.exec', classPattern: '**/classes' , sourcePattern : '**/src/main/java')
                            step([$class: 'Publisher', canRunOnFailed: true, reportFilenamePattern: '**/target/surefire-reports/testng-results.xml'])
                        }
                        failure {
                            script{
                                echo "Publishing verify results"
                            }
                            step([$class: 'CheckStylePublisher', canRunOnFailed: true, pattern: '**/checkstyle-result.xml'])
                            step([$class: 'PmdPublisher', canRunOnFailed: true, pattern: '**/pmd.xml', unstableTotalAll:'0'])
                            step([$class: 'FindBugsPublisher', canRunOnFailed: true, pattern: '**/findbugsXml.xml', unstableTotalAll:'0'])

                            script {
                                echo "Adding comments to jiras"
                                addCommentToMergedJiras("Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) failed when 'Run code checks'. Please go to ${env.BUILD_URL}.")
                            }
                        }
                    }
                }
                stage('Build image') {
                    when {
                        environment name: 'quickPipeline', value: 'false'
                        environment name: 'isService', value: 'true'
                    }
                    steps {
                        script{
                            echo "Building image"
                        }
                        sh "mvn -f pom.xml -e install -DskipTests=true -Pbuild-container-image -Ddocker.image.env=qa -Ddocker.registry.url=781302463931.dkr.ecr.eu-west-1.amazonaws.com"
                    }
                }
                stage('Publish snapshot') {
                    when {
                        environment name: 'quickPipeline', value: 'false'
                        not {
                            anyOf {
                                changeset glob: ".hgtags", caseSensitive: true
                                expression { vcs.isNewRelease(tag)}
                            }

                        }
                    }
                    steps {
                        script{
                            echo "Publishing snapshot"
                        }
                        sh "mvn -e  clean deploy -Ddocker.image.env=qa  -Pbuild-container-image -Ddocker.registry.url=781302463931.dkr.ecr.eu-west-1.amazonaws.com -DskipTests=true"
                    }
                }
                stage('Release') {
                    when {
                        anyOf {
                            changeset glob: ".hgtags", caseSensitive: true
                            expression { vcs.isNewRelease(tag) }
                        }
                    }
                    stages {
                        stage('Get last tag') {
                            steps {
                                script {
                                    echo "Getting last tag"
                                    lastTag = vcs.getLastTag(tag)
                                    echo "Last tag = $lastTag"
                                }
                            }
                        }
                        stage('Update to tag') {
                            steps {
                                script{
                                    echo "update to tag"
                                    vcs.updateToTag(lastTag)
                                }
                            }
                        }
                        stage('Set Version') {
                            steps {
                                script{
                                    echo "Setting release version"
                                }
                                sh "mvn -e versions:set -DgenerateBackupPoms=false -DnewVersion=$lastTag"
                            }
                        }
                        stage('Publish release') {
                            steps {
                                script{
                                    echo "Publishing releases on nexus"
                                }
                                sh "mvn -f pom.xml -Dversion=$lastTag -e clean deploy -Pcreate-release -DskipTests=true"
                            }
                            post {
                                success {
                                    script {
                                        echo "Moving jiras to 'Released' and adding the 'Fix Version'"
                                        addFixVersionAndMoveToReleasedMergedJiras(lastTag)
                                    }
                                }
                                failure {
                                    script {
                                        echo "Adding comments to jiras"
                                        addCommentToMergedJiras("Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) failed when 'Publish release'. Please go to ${env.BUILD_URL}.")
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Publish PROD image') {
                    when {
                        expression { lastTag != null }
                        environment name: 'isService', value: 'true'
                    }
                    steps {
                        script{
                            echo "Publishing PROD releases on registry"
                            echo "DOCKER_GOOGLE_CREDENTIALS: ${DOCKER_GOOGLE_CREDENTIALS}"
                            setServiceAccountFile("edo-prod-google-cloud")
                            if (env.serviceType && serviceType == "Cloud" || serviceType == "Cloud-Mesos" || serviceType == "Cloud-Library") {
                                sh "mvn -f pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=prod -Ddocker.registry.url=eu.gcr.io/edo-prod-resources -DskipTests=true"

                            }
                            if (env.serviceType && serviceType == "Mesos" || serviceType == "Cloud-Mesos") {
                                sh "mvn -f pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=prod -Ddocker.registry.url=registry-rw.services.odigeo.com -DskipTests=true"
                            }
                        }
                    }
                    post {
                        failure {
                            script {
                                echo "Adding comments to jiras"
                                addCommentToMergedJiras("Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) failed when 'Publish PROD image'. Please go to ${env.BUILD_URL}.")
                            }
                        }
                    }
                }

                stage('Publish TEST images (QA)') {
                    when {
                        expression { lastTag != null }
                        environment name: 'isService', value: 'true'
                    }
                    steps {
                        script {
                            echo "Publishing QA releases on registry"
                            setServiceAccountFile("edo-test-google-cloud")
                            sh "mvn -f packaging/container/pom.xml -e clean deploy -Pbuild-container-image -Ddocker.image.env=qa -Ddocker.registry.url=781302463931.dkr.ecr.eu-west-1.amazonaws.com -DskipTests=true"
                            try{
                                sh "mvn com.spotify:docker-maven-plugin:0.4.10:tag -Dimage=${artifactId} -DnewName=781302463931.dkr.ecr.eu-west-1.amazonaws.com/${artifactId}:latest -DpushImage=true -DuseConfigFile=true"
                            }catch (ex){
                                echo "Cannot create latest tag"
                            }
                        }
                    }
                    post {
                        failure {
                            script {
                                echo "Adding comments to jiras"
                                addCommentToMergedJiras("Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) failed when 'Publish TEST image (QA)'. Please go to ${env.BUILD_URL}.")
                            }
                        }
                    }
                }
                stage('Create Jira') {
                    when { expression {lastTag != null} }
                    steps {
                        script {
                            echo "Creating 'Deployment' jira"
                            try{
                                deploymentIssueId = createDeploymentJiraAndLinkIt(lastTag,cIDate,funcionalTestDate)
                            }catch(ex) {
                                echo "cannot create jira"
                                ex.getMessage()
                            }
                        }
                    }
                }
                stage('Increase upstream version') {
                    when { expression {lastTag != null} }
                    steps {
                        script {
                            try{
                                vcs.revertLocalChangesAndUpdate()
                                sh "mvn -e versions:set -DgenerateBackupPoms=false -DnewVersion=$lastTag"
                                sh "echo Increasing upstream version"
                                sh "mvn build-helper:parse-version versions:set -DgenerateBackupPoms=false -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.nextMinorVersion}.0-SNAPSHOT versions:commit"
                                vcs.commit("Set new version from pipeline")
                                vcs.mergeAndPush()
                            } catch (ex){
                                echo "cannot update upstream version"
                                ex.getMessage()
                            }
                        }
                    }
                }
                stage('Deploy') {
                    when {
                        environment name: 'isService', value: 'true'
                        anyOf {
                            environment name: 'serviceType', value: 'Cloud-Mesos'
                            environment name: 'serviceType', value: 'Cloud'
                        }
                        expression {lastTag != null}
                    }
                    stages {
                        stage('Check SQL scripts') {
                            steps {
                                script {
                                    if (showScriptLayerInput(lastTag)) {
                                        timeout(time: calculateWaitingTimeUntilAbortPromoteInSeconds(), unit: 'SECONDS') {
                                            input(id: 'proceed-to-canary', message: 'There are SQL scripts in this version. Are all of them executed in PROD?' +
                                                    '\n\n*You have until 19h to proceed (or 10min if you are deploying after 19h), otherwise it will be aborted automatically')
                                        }
                                    }
                                }
                            }
                        }
                        stage('Checkout') {
                            steps {
                                script{
                                    vcs.checkout("${artifactId}-platform")
                                }
                            }
                        }
                        stage ('Ask before deploy to canary') {
                            when { expression {getPropertyFromPipelineConfiguration("askCanary", "false") == "true"} }
                            steps{
                                script {
                                    timeout(time: calculateWaitingTimeUntilAbortPromoteInSeconds(), unit: 'SECONDS') {
                                        input(id: 'proceed-to-canary', message: 'Do you want to deploy to canary? ')
                                    }
                                }
                            }
                        }
                        stage('Get deployment versions') {
                            steps {
                                script {
                                    deployType = new DeployTypeFactory(steps).getInstance(env.deployType)
                                    if (deployType.hasCanary()) {
                                        previousCanaryVersion = deployType.getCanaryVersionFromDeploymentFile("prod")
                                        echo "previous canary: ${previousCanaryVersion}"
                                    }
                                    previousStableVersion = deployType.getStableVersionFromDeploymentFile("prod")
                                    echo "previous stable: ${previousStableVersion}"
                                }
                            }
                        }
                        stage('Deploy to Canary') {
                            when { expression {deployType.hasCanary() == true} }
                            steps {
                                script {
                                    echo "Deploying to canary"
                                    deployType.updateCanaryDeploymentFile("prod", lastTag)
                                    vcs.commitAndPushIfChanges("update prod canary version to ${lastTag}")
                                    updateFixVersion(lastTag,true,true) //set version as released and archivated
                                    sendStartDeployEvent(artifactId, previousStableVersion, lastTag, "CANARY", vcs.getLastCommitHash())
                                    build job: "${artifactId}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToTest', value: false)]
                                }
                            }
                        }

                    }
                }
            }
        }
        stage ('Check and promote') {
            agent {
                label "master"
            }
            when {
                environment name: 'isService', value: 'true'
                anyOf {
                    environment name: 'serviceType', value: 'Cloud-Mesos'
                    environment name: 'serviceType', value: 'Cloud'
                }
                expression {lastTag != null}
            }
            stages {
                stage('BetaTool waiting for samples') {

                    when { expression {deployType.hasCanary() == true} }
                    steps {
                        script{
                            if(env.teamEmail){
                                def result = getBetaToolRecommendationAndNotify(artifactId, lastTag, previousStableVersion, teamEmail, env.BUILD_URL, DEPLOY_REPORTER_URL)
                                if( result.recommendation == "INCOMPARABLE"){
                                    currentBuild.result = 'ABORTED'
                                    error("Found incomparable version between Canary and Stable. Review the deployment status in Google console and fix it with a manual deploy if needed")
                                }
                                if (result.recommendation != "PROMOTE"){
                                    currentBuild.result = 'UNSTABLE'
                                }
                                evaluationUrl = result.evaluationUrl
                            }else{
                                echo "No team email setted. Skip this stage"
                            }
                        }
                    }
                }
                stage('Promote to Stable') {
                    when { expression {deployType.hasCanary() == true} }
                    steps {
                        promoteToStableWithoutTimeout(deploymentIssueId, env.BUILD_URL, evaluationUrl)
                    }
                }
            }
        }
        stage ('Deploy to stable and test'){
            agent {
                label "GeneralLibraryInstance"
            }
            when {
                environment name: 'isService', value: 'true'
                anyOf {
                    environment name: 'serviceType', value: 'Cloud-Mesos'
                    environment name: 'serviceType', value: 'Cloud'
                }
                expression {lastTag != null}
            }
            stages {
                stage('Checkout') {
                    steps {
                        script{
                            vcs.checkout("${artifactId}-platform")
                        }
                    }
                }
                stage('Deploy to Stable') {
                    steps {
                        script {
                            echo "Deploying to stable"
                            deployType.updateStableDeploymentFile("prod",lastTag)
                            vcs.commitAndPushIfChanges("update prod stable version to ${lastTag}")
                            sendStartDeployEvent(artifactId, lastTag, lastTag, "STABLE", vcs.getLastCommitHash())
                            build job: "${artifactId}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToTest', value: false)]
                        }
                    }
                }
                stage('Deploy to TEST') {
                    steps {
                        script {
                            echo "Deploying to test"
                            if (deployType.hasCanary()) {
                                deployType.updateCanaryDeploymentFile("test", lastTag)
                            }
                            deployType.updateStableDeploymentFile("test",lastTag)
                            vcs.commitAndPushIfChanges("update test version to ${lastTag}")
                            sendStartDeployTestEvent(artifactId, lastTag, vcs.getLastCommitHash())
                            build job: "${artifactId}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToProd', value: false)]
                        }
                    }
                }
            }

        }
    }
}

def getRepositoryName(){
    getPropertyFromPipelineConfiguration("repository", artifactId)

}

def sendStartDeployEvent(String module, String stableVersion, String canaryVersion, String partition, String commit) {
	sendEventToDeployReporter(module, "START-DEPLOY-CD-" + partition, stableVersion, canaryVersion, "Deploy from CD pipeline; Platform: " + commit, DEPLOY_REPORTER_URL)
}

def sendStartDeployTestEvent(String module, String version, String commit) {
	sendEventToDeployReporter(module, "START-DEPLOY-CD-TO-TEST", version, version, "Deploy from CD pipeline; Platform: " + commit, DEPLOY_REPORTER_URL)
}

def getNowTimestapAsString() {
	return new Date().format("dd-MM-yyyy HH:mm", TimeZone.getTimeZone('Europe/Madrid'))
}

def addCommentToMergedJiras(comment) {
	try {
		def searchResults = jiraJqlSearch(jql: "project = '${jiraProjectId}' AND issuetype = Delivery AND status = Merged")
		def issues = searchResults.data.issues
		for (i = 0; i < issues.size(); i++) {
			jiraAddComment(idOrKey: issues[i].key, comment: comment, auditLog: false)
			echo "Added comment to ${issues[i].key}"
		}
	} catch (ex) {
		echo "cannot add comment to merged jiras"
		ex.getMessage()
	}
}

def addFixVersionAndMoveToReleasedMergedJiras(version){
	try{
		//Get all jiras in merge status
		def searchResults = jiraJqlSearch(jql: "project = '${jiraProjectId}' AND issuetype = Delivery AND status = Merged", failOnError: false)
		def issues = searchResults.data.issues

		addFixVersion(issues, version)
		moveToReleased(issues)
	} catch (ex){
		echo "cannot add fix version to merged jiras and move it to released"
		ex.getMessage()
		currentBuild.result = 'UNSTABLE'
	}
}

def addFixVersion(issues, version) {

	def fixVersion = createFixVersionIfNecessary(version);

    for (i = 0; i < issues.size(); i++) {
		def issueId = issues[i].key
		def testIssue = [fields: [fixVersions: [fixVersion]]]
		jiraEditIssue(idOrKey: issueId, issue: testIssue, failOnError: false)

		echo "Added fix version to ${issueId}"
    }
}

def moveToReleased(issues) {
	def transitionInput
    for (i = 0; i < issues.size(); i++) {
		def issueId = issues[i].key
		if (transitionInput == null) {
			transitionInput = getTransition(issueId, 'Released')
		}
		jiraTransitionIssue(idOrKey: issueId, input: transitionInput, failOnError: false)

		echo "${issueId} moved to Released"
    }
}

def getTransition(issueId, transitionName) {
	def transitionResult = jiraGetIssueTransitions(idOrKey: issueId, failOnError: false)
	def transitions = transitionResult.data.transitions
	for (j = 0; j < transitions.size(); j++) {
		if (transitions[j].name == transitionName) {
			return [transition: transitions[j]]
		}
	}
	return null
}


def createDeploymentJiraAndLinkIt(String version, String cIDate, String funcionalTestDate){
	def issueKey =createDeploymentJira(lastTag, "CI passed on: ${cIDate}; Funcionals Tests passed on: ${funcionalTestDate}")
	def jirasToLink = getJirasInReleased(version)
	linkJiras(issueKey, jirasToLink)
}

def linkJiras(issueKey, issues) {
    for (i = 0; i < issues.size(); i++) {
		jiraLinkIssues(type: 'Release', inwardKey: issueKey, outwardKey: issues[i].key, failOnError: false)

		echo "Linked ${issueKey} to ${issues[i].key}"
    }
}

def getJirasInReleased(version){
    def searchResults = jiraJqlSearch(jql: "project = '${jiraProjectId}' AND issuetype = Delivery AND status = 'Released' AND fixVersion  = '${version}'", failOnError: false)
    def issues = searchResults.data.issues

	return issues
}

def archiveLogs(){
    archiveArtifacts(artifacts: "**/*.log", allowEmptyArchive: true)
}


def showScriptLayerInput(version) {
	def files = findFiles glob: "**/$version/**/*.sql"
	boolean hasScripts = files.length > 0
	if (hasScripts) {
		for (int i = 0; i < files.length; i++) {
			if (!"doNotRemoveThisFile.sql".equalsIgnoreCase(files[i].name)) {
				return true
			}
		}
	}
	return false
}