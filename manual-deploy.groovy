@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory
import com.odigeo.pipeline.vcs.VcsFactory
import com.odigeo.pipeline.vcs.Vcs

pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    parameters {
        string(defaultValue: '', description: 'version to deploy in canary', name: 'canaryVersion')
        string(defaultValue: '', description: 'version to deploy in stable', name: 'stableVersion')
    }
    environment {
        DEPLOY_REPORTER_URL= "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"
        Vcs vcs = null
    }
        stages {
        stage('Checkout') {
            steps {
                script{
                    vcs = new VcsFactory(steps).getInstance("${artifactId}-platform")
                    vcs.checkout("${artifactId}-platform")
                }
            }
        }
        stage('Get deployment versions') {
            steps {
                script {
                    deployType = new DeployTypeFactory(steps).getInstance(env.deployType)
                    if (deployType.hasCanary()) {
                        previousCanaryVersion = deployType.getCanaryVersionFromDeploymentFile("prod")
                        echo "previous canary: ${previousCanaryVersion}"
                    }
                    previousStableVersion = deployType.getStableVersionFromDeploymentFile("prod")
                    echo "previous stable: ${previousStableVersion}"
                }
            }
        }
        stage('Manual deploy to Prod') {
            steps {
                script {
                    if (canaryVersion != '') {
                        echo "Change canary version"
                        updateDeploymentFile("prod", canaryVersion, "canary")
                    }
                    if (stableVersion != '') {
                        echo "Change stable version"
                        updateDeploymentFile("prod", stableVersion, "stable")
                    }
                    if (canaryVersion != '' || stableVersion != '') {
                        vcs.commitAndPushIfChanges("set canary to ${canaryVersion} and  stable version in prod ${stableVersion} with manual deploy")
                        sendStartDeployEvent(artifactId, canaryVersion, stableVersion,"PROD", vcs.getLastCommitHash())
                        build job: "${artifactId}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToTest', value: false)]
                    }
                }
            }
        }
        stage ('Manual deploy to Test') {
            when { expression {params.stableVersion != ''}}
            steps {
                script{
                    updateDeploymentFile("test", stableVersion, "canary")
                    updateDeploymentFile("test", stableVersion, "stable")
                    vcs.commitAndPushIfChanges("set canary to ${canaryVersion} and  stable version ${stableVersion} in Test with manual deploy")
                    sendStartDeployEvent(artifactId, stableVersion, stableVersion,"TEST", vcs.getLastCommitHash())
                    build job: "${artifactId}-platform", propagate: true, wait: true,  parameters: [booleanParam(name: 'deployToProd', value: false)]
                }
            }

        }
    }
}

def sendStartDeployEvent(String module, String stableVersion, String canaryVersion, String environment, String commit) {
    sendEventToDeployReporter(module, "START-MANUAL-DEPLOY-${environment}", stableVersion, canaryVersion, "Manual Deploy; Platform: " + commit, DEPLOY_REPORTER_URL)
}










