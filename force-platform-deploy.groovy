@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory
@Library('deploy-pipeline-shared-libraries')
import com.odigeo.pipeline.deploy.DeployTypeFactory
import com.odigeo.pipeline.vcs.Vcs
import com.odigeo.pipeline.vcs.VcsFactory

pipeline {
    agent {
        label 'GeneralLibraryInstance'
    }
    parameters {
        string(defaultValue: '',description: 'module to deploy', name: 'module')
        choice(choices: ['Default', 'SparkRobot'], description: 'Deploy Type', name: 'deployType')
    }
    environment {
        DEPLOY_REPORTER_URL= "http://haproxy-deploy-reporter.service.bcn1.edo.sv:8099"
        Vcs vcs = null
    }
    stages {
        stage('Checkout') {
            steps {
                script{
                    vcs = new VcsFactory(steps).getInstance("${module}-platform")
                    vcs.checkout("${module}-platform")
                }

            }
        }
        stage('Force Platform Deploy') {
            steps {
                script {
                    deployType = new DeployTypeFactory(steps).getInstance(deployType)
                    sendStartDeployEvent(module, vcs.getLastCommitHash(), deployType)
                    build job: "${module}-platform", propagate: true, wait: true
                }
            }
        }
    }
}

def sendStartDeployEvent(String module, String commit, deployType) {
    canaryVersion = null
    if (deployType.hasCanary()) {
        canaryVersion = deployType.getCanaryVersionFromDeploymentFile("prod")

    }
    stableVersion = deployType.getStableVersionFromDeploymentFile("prod")
    sendEventToDeployReporter(module, "START-FORCE-PLATFORM-DEPLOY", stableVersion, canaryVersion, "Force Platform deploy with : " + commit, DEPLOY_REPORTER_URL)
}











